import * as React from "react";

export enum Status{
    Alive, PartiallyAlive, Broken, NoData
}

export class EnvironmentStatus extends React.Component<any, any>{
    render() {
        let className = "";
        let flatClass = this.props.isFlat? "--flat" : "";
        switch (this.props.status) {
            case Status.Alive:
                className="environment-status--alive"+flatClass;
                break;
            case Status.PartiallyAlive:
                className="environment-status--partially-alive"+flatClass;
                break;
            case Status.Broken:
                className="environment-status--broken"+flatClass;
                break;
            case Status.NoData:
                className="environment-status--no-data"+flatClass;
                break;
            default:
                className="environment-status--no-data"+flatClass;
                break;
        }
        return (
            <span className={className}></span>
        );
    }
}