import * as React from "react";
import * as MUI from "material-ui";
import {RouteHandler, Link} from "react-router";
import * as ChartJS from "react-chartjs";

import {ProductBriefStatistics} from "../models/ProductBriefStatistics";
import {BriefStatistics} from "../models/BriefStatistics";
import {BriefModulesStats} from "./brief-modules-stats";

//used MUI components
let ThemeManager = new MUI.Styles.ThemeManager();

export class ProductBriefCard extends React.Component<any, ProductBriefStatistics>{
    static childContextTypes = {
        muiTheme: React.PropTypes.object
    }
    
    constructor(){
        this.props = {"product": new ProductBriefStatistics()};
        super();
    }
    
    getChildContext() {
        return {
            muiTheme: ThemeManager.getCurrentTheme()
        };
    }
    
    render(){
        let stats = this.props.product.statistics;
        let totalModulesCount = stats.availableModulesCount + stats.brokenModulesCount + stats.undefinedModulesCount;
        let availableModulesPercentage = Math.round(stats.availableModulesCount / totalModulesCount * 100);
        let brokenModulesPercentage = Math.round(stats.brokenModulesCount / totalModulesCount * 100);
        let undefinedModulesPercentage = Math.round(stats.undefinedModulesCount / totalModulesCount * 100);
        
        let pieChartData = [
            {
                value: availableModulesPercentage,
                color:"#689F38",
                highlight: "#FF5A5E",
            },
            {
                value: brokenModulesPercentage,
                color: "#F44336",
                highlight: "#5AD3D1",
            },
            {
                value: undefinedModulesPercentage,
                color: "#9e9e9e",
                highlight: "#FFC870",
            }
        ];
        let pieChartOptions = { 
            responsive: true,
            animation:true,
            animationSteps : 50,
            showScale: false,
            maintainAspectRatio: false,
            percentageInnerCutout : 80
        };
        
        let lineChartData = {
            labels: ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"],
            datasets: [
                {
                    label: "24h",
                    fillColor: "rgba(104,159,56,0.5)",
                    strokeColor: "rgba(104,159,56,1)",
                    pointColor: "rgba(104,159,56,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: stats.availability24h
                }
            ]
        };
        
        let lineChartOptions = { 
            responsive: true,
            animation:false,
            maintainAspectRatio: false,
            scaleShowGridLines: true,
            bezierCurve: true,
            scaleFontSize: 10,
            pointDot: false,
            showTooltips: false,
            scaleBeginAtZero: true
        };
        
        return (
            <MUI.Card className="product-brief-card card" initiallyExpanded={true}>
                <MUI.CardHeader
                    title={this.props.product.env}
                    subtitle={this.props.product.productName}
                    avatar={
                        <MUI.Avatar 
                            icon={
                                    <MUI.FontIcon className="material-icons">cloud_queue</MUI.FontIcon>
                            }
                            color={MUI.Styles.Colors.wight}
                            backgroundColor={MUI.Styles.Colors.grey300}
                        />
                    }
                    showExpandableButton={true}
                    className="product-brief-card__card-header">
                </MUI.CardHeader>
                <MUI.CardText expandable={true}>
                    <div className="clearfix">
                        <div className="product-brief-card__left-panel">
                            <div style={{width:"100% !important", height:"auto !important"}}>
                                <ChartJS.Doughnut 
                                    data={pieChartData}
                                    options={pieChartOptions}
                                    className="product-brief-card__left-panel__pie-chart" 
                                />
                                <div className="product-brief-card__left-panel__counter-wrapper">
                                    <span className="product-brief-card__left-panel__counter">
                                        {availableModulesPercentage}
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="product-brief-card__right-panel">
                            <BriefModulesStats
                                title = "available modules"
                                percentageValue = { availableModulesPercentage }
                                value = { this.props.product.statistics.availableModulesCount }
                                className = "brief-modules-stats-success"
                            />
                            <BriefModulesStats
                                title = "broken modules"
                                percentageValue = { brokenModulesPercentage }
                                value = { this.props.product.statistics.brokenModulesCount }
                                className = "brief-modules-stats-warning"
                            />
                            <BriefModulesStats
                                title = "no data"
                                percentageValue = { undefinedModulesPercentage }
                                value = { this.props.product.statistics.undefinedModulesCount }
                                className = "brief-modules-stats-default"
                            />
                        </div>
                    </div>
                    <div className="text-center">
                        <div>AVAILABILITY • 24H</div>
                        <ChartJS.Line 
                            data = { lineChartData }
                            options = { lineChartOptions }
                        />
                    </div>
                    <div className="product-brief-card__timestamp">
                        09/07/2015 11:23:44 am
                    </div>
                    <div className="product-brief-card__more-info">
                        <Link to="modules" className="app-bar__links__item">
                            <MUI.IconButton
                                tooltipPosition="top-center"
                                tooltip="More info"
                                style={{margin:"0"}}
                                iconStyle={{color: "#ff8000"}}>
                                <MUI.FontIcon className="material-icons">info</MUI.FontIcon>
                            </MUI.IconButton>
                        </Link>
                    </div>
                </MUI.CardText>
            </MUI.Card>
        );
    }
}