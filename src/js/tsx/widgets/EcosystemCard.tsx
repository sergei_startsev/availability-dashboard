import * as React from "react";

import {Card} from "./Card";
import {EnvironmentStatus, Status} from "../environment-status";

export class EcosystemCard extends React.Component<any, any>{

    constructor(props) {
        super(props);
    }
    
    render() {
        //extract needed field with data from props
        var statistics = this.props.data.statistics;
        
        //CSS classes for environment statuses
        let cssClasses = {
            0 : "env-info-header__value env-info-header__value_alive",
            1 : "env-info-header__value env-info-header__value_partially-alive",
            2 : "env-info-header__value env-info-header__value_broken",
            3 : "env-info-header__value env-info-header__value_no-data"
        };
        
        return (
            <Card
                className = {this.props.className}
                header = {this.props.header}
                footer = {this.props.footer}
            >
                <div className = {this.props.innerClassName}>
                    <table className="w-ecosystem-card__env-info">
                        <tr>
                            <td className="env-info-header f-weight-400 text-center">
                                <span>CI</span>
                            </td>
                            <td className="env-info-header f-weight-400 text-center">
                                <span>DEMO</span>
                            </td>
                            <td className="env-info-header f-weight-400 text-center">
                                <span>QED</span>
                            </td>
                            <td className="env-info-header f-weight-400 text-center">
                                <span>
                                    <i className="material-icons f-large-3 text-alert-color">whatshot</i>
                                    <span className="f-small-5">PROD</span>
                                </span>
                            </td>
                            <td className="env-info-header f-weight-400 text-center">
                                <span>PROD</span>
                            </td>
                        </tr>
                        <tr>
                            <td className="env-info-header text-center">
                                <EnvironmentStatus status={statistics.ci.status}/>
                            </td>
                            <td className="env-info-header text-center">
                                <EnvironmentStatus status={statistics.demo.status}/>
                            </td>
                            <td className="env-info-header text-center">
                                <EnvironmentStatus status={statistics.qed.status}/>
                            </td>
                            <td className="env-info-header text-center">
                                <EnvironmentStatus status={statistics.hotprod.status}/>
                            </td>
                            <td className="env-info-header text-center">
                                <EnvironmentStatus status={statistics.prod.status}/>
                            </td>
                        </tr>
                        <tr>
                            <td className="env-info-header text-center">
                                {(() => {
                                    if (statistics.ci.status != Status.NoData) {
                                        return (
                                            <span className={cssClasses[statistics.ci.status]}>
                                                <span>{statistics.ci.availability}</span>
                                                <span className="f-small-3">%</span>
                                                </span>
                                        )
                                    } else {
                                        return (<span>?</span>)
                                    }
                                })() }
                            </td>
                            <td className="env-info-header text-center">
                                {(() => {
                                    if (statistics.demo.status != Status.NoData) {
                                        return (
                                            <span className={cssClasses[statistics.demo.status]}>
                                                <span>{statistics.demo.availability}</span>
                                                <span className="f-small-3">%</span>
                                                </span>
                                        )
                                    } else {
                                        return (<span>?</span>)
                                    }
                                })() }
                            </td>
                            <td className="env-info-header text-center">
                                {(() => {
                                    if (statistics.qed.status != Status.NoData) {
                                        return (
                                            <span className={cssClasses[statistics.qed.status]}>
                                                <span>{statistics.qed.availability}</span>
                                                <span className="f-small-3">%</span>
                                                </span>
                                        )
                                    } else {
                                        return (<span>?</span>)
                                    }
                                })() }
                            </td>
                            <td className="env-info-header text-center">
                                {(() => {
                                    if (statistics.hotprod.status != Status.NoData) {
                                        return (
                                            <span className={cssClasses[statistics.hotprod.status]}>
                                                <span>{statistics.hotprod.availability}</span>
                                                <span className="f-small-3">%</span>
                                                </span>
                                        )
                                    } else {
                                        return (<span>?</span>)
                                    }
                                })() }
                            </td>
                            <td className="env-info-header text-center">
                                {(() => {
                                    if (statistics.prod.status != Status.NoData) {
                                        return (
                                            <span className={cssClasses[statistics.prod.status]}>
                                                <span>{statistics.prod.availability}</span>
                                                <span className="f-small-3">%</span>
                                                </span>
                                        )
                                    } else {
                                        return (<span>?</span>)
                                    }
                                })() }
                            </td>
                        </tr>
                    </table>
                </div>
            </Card>
        );
    }
}

EcosystemCard.defaultProps = {
    innerClassName: "w-ecosystem-card"
};