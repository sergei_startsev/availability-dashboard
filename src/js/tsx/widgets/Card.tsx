import * as React from "react";

export class Card extends React.Component<any, any>{

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className = {this.props.className}>
                {this.props.header}
                <div className="w-card__body">
                    {this.props.children}
                </div>
                {this.props.footer}
            </div>
        );
    }
}

Card.defaultProps = {
    className: "w-card"
};