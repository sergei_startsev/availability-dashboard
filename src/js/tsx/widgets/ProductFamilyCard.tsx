import * as React from "react";
import * as ChartistGraph from "react-chartist";

import {Card} from "./Card";
import {BriefModulesStats} from "../brief-modules-stats";
import {ProductBriefStatistics} from "../../models/ProductBriefStatistics";

export class ProductFamilyCard extends React.Component<any, any>{

    constructor(props) {
        super(props);
    }
    
    private generateLineChartLabels(): string[]{
        let labels: string[] =[];
        for (let i = 0; i < 24; i++) {
            if (i < 10) {
                labels.push("0" + i);
            } else {
                labels.push(i.toString());
            }
        }
        return labels;
    } 
    
    render() {
        let stats = this.props.productFamily.statistics;
        let totalModulesCount = stats.availableModulesCount + stats.brokenModulesCount 
            + stats.undefinedModulesCount;
        let availableModulesPercentage = Math.round(stats.availableModulesCount / totalModulesCount * 100);
        let brokenModulesPercentage = Math.round(stats.brokenModulesCount / totalModulesCount * 100);
        let undefinedModulesPercentage = Math.round(stats.undefinedModulesCount / totalModulesCount * 100);
        
        return (
            <Card
                className = {this.props.className}
                header = {this.props.header}
                footer = {this.props.footer}
            >
                <div className = {this.props.innerClassName}>
                    <div className="clearfix">
                        <div className="w-product-family-card__pie-chart">
                            <ChartistGraph 
                                type={'Pie'} 
                                data={
                                    {
                                        series: [
                                            availableModulesPercentage,
                                            brokenModulesPercentage,
                                            undefinedModulesPercentage
                                        ],
                                        labels: ["available", "broken", "undefined"]
                                    }
                                }
                                options={
                                    {
                                        donut: true, donutWidth: 10, chartPadding:0, showLabel: false,
                                        classNames: {series: 'pie-series'}
                                    }
                                }
                            />
                            <div className="w-product-family-card__pie-chart-indicator">
                                <span className="pie-chart-indicator">
                                    {availableModulesPercentage}
                                </span>
                            </div>
                        </div>
                        <div className="w-product-family-card__chart-info">
                            <BriefModulesStats
                                title = "available modules"
                                percentageValue = { availableModulesPercentage }
                                value = { stats.availableModulesCount }
                                className = "brief-modules-stats-success"
                            />
                            <BriefModulesStats
                                title = "broken modules"
                                percentageValue = { brokenModulesPercentage }
                                value = { stats.brokenModulesCount }
                                className = "brief-modules-stats-warning"
                            />
                            <BriefModulesStats
                                title = "no data"
                                percentageValue = { undefinedModulesPercentage }
                                value = { stats.undefinedModulesCount }
                                className = "brief-modules-stats-default"
                            />
                        </div>
                    </div>
                    <div className="text-center">
                        <div>AVAILABILITY • 24H <i className="pe-7s-clock pe-lg"/></div>
                        <ChartistGraph 
                            type = {'Line'}
                            data = {{ labels: this.generateLineChartLabels(), series: [stats.availability24h]}} 
                            options = {{
                                showArea: true,
                                fullWidth: true,
                                chartPadding: {
                                    top: 15,
                                    right: 10,
                                    bottom: 10,
                                    left: 10
                                },
                                axisX: {offset: 10},
                                axisY: {offset: 20},
                                classNames: {series: 'area-series', label: 'area-label',}
                            }}
                        />
                    </div>
                </div>
            </Card>
        );
    }
}

ProductFamilyCard.defaultProps = {
    innerClassName: "w-product-family-card",
    productFamily: new ProductBriefStatistics()
};