import * as React from "react";
import {TrendArrow} from "./trend-arrow";

interface Props {
    value: string;
    showPercentage: boolean;
    showTrend: boolean;
    className: string
}

interface State {
    trend: TrendEnum;
}

export enum TrendEnum { Up, Down, Unchanged }

export class TrackedValue extends React.Component<Props, State>{

    constructor() {
        this.state = { trend: TrendEnum.Up };
        this.props = { value: "", showPercentage: false, showTrend: false }
        super();
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.showTrend) {
            if (nextProps.value > this.props.value) {
                this.setState({
                    trend: TrendEnum.Up
                });
            } else {
                if (nextProps.value < this.props.value) {
                    this.setState({
                        trend: TrendEnum.Down
                    });
                } else {
                    this.setState({
                        trend: TrendEnum.Unchanged
                    });
                }
            }
        }
    }

    render() {
        return (
            <div className={ this.props.className } >
                <span className="tracked-value"> { this.props.value } </span>
                { this.props.showPercentage ? <span className="tracked-value__percent">%</span> : null }
                {(() => {
                    if (this.props.showTrend && this.state.trend != TrendEnum.Unchanged) {
                        return (
                            <TrendArrow value={ this.state.trend } />
                        );
                    }
                })()}
            </div>
        );
    }

}