import * as React from "react";
import * as MUI from "material-ui";
import * as Socket from "socket.io-client";

import {ProductBriefCardsList} from "./product-brief-cards-list";

//used MUI components
let ThemeManager = new MUI.Styles.ThemeManager();
let Card = MUI.Card;

export class ProductBriefCardsListWrapper extends React.Component<any, any>{
    static childContextTypes = {
        muiTheme: React.PropTypes.object
    }
    
    constructor(){
        this.state = {"products": []};
        super();
    }
    
    getChildContext() {
        return {
            muiTheme: ThemeManager.getCurrentTheme()
        };
    }
    
    componentDidMount() {
        let socket = Socket(document.location.hostname + ":3000");
        socket.on('briefStatistics', (data)=>{
            //console.debug(data);
            this.setState({"products": data});
        });
    }
    
    render(){
        //console.debug("Render: "+this.state.products);
        return(
            <div>
                {
                    this.state.products.length>0 
                    ? <ProductBriefCardsList products={this.state.products}/> 
                    : null
                }
            </div>
        );
    }
}