/// <reference path="../../../typings/react/react.d.ts"/>

import * as React from "react";

/**
 * Column
 */
export class Column extends React.Component<any, void>{
    render() {
        return (
            <div className={"columns " + this.props.className}>
                {this.props.children}
            </div>
        );
    }
}