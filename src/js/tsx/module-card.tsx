import * as React from "react";
import * as MUI from "material-ui";
import * as Socket from "socket.io-client";
import * as ChartistGraph from "react-chartist";

import {Label} from "./components/Label";

export class ModuleCard extends React.Component<any, any>{
    constructor(props){
        this.state = {"data": []};
        super(props);
    }
    //TODO REMOVE IT
    private getRandom(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min + 1) + min);;
    }
    //TODO REMOVE IT
    private generateFakeData() {
        let data = { labels: [], series: [] }
        let dataSerie = [];
        for (let i = 1; i <= 24; i++) {
            let hours;
            if (i <= 12) {
                hours = i + "am";
            } else {
                hours = i - 12 + "pm";
            }
            data.labels.push(hours);
            if (this.getRandom(0, 10) < 8) {
                dataSerie.push(100);
            } else {
                dataSerie.push(this.getRandom(0, 100));
            }
        }

        let dataSerie2 = [], dataSerie3 = [];
        for (let i = 0; i < 24; i++) {
            let diff = 100 - dataSerie[i];
            if (diff > 0) {
                if (this.getRandom(0, 10) < 8) {
                    dataSerie2.push(diff);
                    dataSerie3.push(0);
                } else {
                    dataSerie2.push(0);
                    dataSerie3.push(diff);
                }
            } else {
                dataSerie2.push(0);
                dataSerie3.push(0);
            }
        }
        data.series.push(dataSerie);
        data.series.push(dataSerie2);
        data.series.push(dataSerie3);


        let data2 = { labels: data.labels, series: [] }
        let data3 = { labels: data.labels, series: [] }
        dataSerie = [], dataSerie2 = [], dataSerie3 = [];
        let dataSerie4 = [], dataSerie5 = [], dataSerie6 = [];
        for (let i = 0; i < 24; i++) {
            let success = data.series[0][i];
            let fail = data.series[1][i];
            let unknown = data.series[2][i];
            if (success != 100) {
                if (success <= 50) {
                    dataSerie[i] = success * 2;
                    dataSerie4[i] = 0;
                } else {
                    dataSerie[i] = 100;
                    dataSerie4[i] = success * 2 - 100;
                }
                if (fail * 2 + dataSerie[i] <= 100) {
                    dataSerie2[i] = fail * 2;
                    dataSerie5[i] = 0;
                } else {
                    dataSerie2[i] = 100 - dataSerie[i];
                    dataSerie5[i] = fail * 2 - (100 - dataSerie[i]);
                }

                if (unknown * 2 + dataSerie[i] <= 100) {
                    dataSerie3[i] = unknown * 2;
                    dataSerie6[i] = 0;
                } else {
                    dataSerie3[i] = 100 - dataSerie[i];
                    dataSerie6[i] = unknown * 2 - (100 - dataSerie[i]);
                }
            } else {
                dataSerie[i] = 100;
                dataSerie2[i] = 0;
                dataSerie3[i] = 0;
                dataSerie4[i] = 100;
                dataSerie5[i] = 0;
                dataSerie6[i] = 0;
            }
        }

        data2.series.push(dataSerie);
        data2.series.push(dataSerie2);
        data2.series.push(dataSerie3);

        data3.series.push(dataSerie4);
        data3.series.push(dataSerie5);
        data3.series.push(dataSerie6);

        return { data: data, data2: data2, data3: data3 };
    }
    
    componentDidMount() {
        let socket = Socket(document.location.hostname + ":3000");
        socket.on('modulesStatistics', (data)=>{
            //console.debug(data);
            this.setState({"data": data});
        });
    }
    
    private timeConverter(timestamp): string {
        var a = new Date(timestamp);
        var year = a.getFullYear();
        var month = a.getMonth();
        var date = a.getDate();
        var hour = a.getHours();
        var min = a.getMinutes();
        var sec = a.getSeconds();
        var time = month + '/' + date + '/' + year + ' ' + hour + ':' + min + ':' + sec;
        return time;
    }
    
    render() {
        let options = {
            stackBars: true,
            axisX: {
                showGrid: false,
                offset:20
            },
            axisY: {
                showGrid: false
            },
            chartPadding: {
                top: 20,
                bottom: 0,
            },
        };
        
        let standardActions = [
            { text: 'OK' }
        ];
        
        return (
            <div>
            {
                (()=>{
                    if(this.state.data.length>0 ){
                        return this.state.data.map((module, i)=>{
                            //console.debug(this.props.logInfo);
                            return (
                                <MUI.Card key={i} className="module-card card" initiallyExpanded={true}>
                                <MUI.CardHeader
                                    title={module.moduleName + " Module"}
                                    subtitle={module.productFamily + " Product Family"}
                                    avatar={
                                        <Label value = {module.moduleName.substring(0,1)} className="c-label c-label_inherit text-light-grey-color f-large-12"/>
                                    }
                                    showExpandableButton={true}
                                />
                                <MUI.CardText expandable={true}>
                                    {
                                        module.environments.map((env, j)=>{
                                            return (
                                                <MUI.Card key={i+"_"+j} className="module-card card" initiallyExpanded={true}>
                                                    <MUI.CardHeader
                                                        subtitle={
                                                            <div className="module-card__subtitle">
                                                                <div>
                                                                    VERSION: <Label value = {env.version} className="c-label c-label_primary c-label_inherit f-small-1" />
                                                                </div>
                                                                <div>
                                                                    CREATED ON: <Label value = {this.timeConverter(env.date)} className="c-label c-label_light c-label_inherit f-small-2" />
                                                                </div>
                                                            </div>
                                                        }
                                                        avatar={
                                                            <Label value = {env.envName} className="c-label c-label_inherit text-grey-color f-large-12"/>
                                                        }
                                                        showExpandableButton={true}
                                                    />
                                                    <MUI.CardText expandable={true}>
                                                        <div className="module-card__type">
                                                            MODULE TYPE: <Label value = {module.type} className="c-label c-label_light c-label_inherit f-small-1" />
                                                        </div>
                                                        {
                                                            env.availability.map((availability, k)=>{
                                                                return (
                                                                    <div key={i+"_"+j+"_"+k} className="clearfix">
                                                                        <div className="module-card__section-title">
                                                                            {(()=>{
                                                                            if(typeof availability.server != "undefined"){
                                                                                return (
                                                                                    <div>
                                                                                        BY SERVER
                                                                                        <div className="module-card__server-name">
                                                                                            <span className="module-card__label--green">ACTIVE</span> {availability.name}
                                                                                        </div>
                                                                                    </div>
                                                                                )
                                                                            }else{
                                                                                return (
                                                                                    <div>
                                                                                        {availability.name}
                                                                                    </div>
                                                                                )
                                                                            }
                                                                            })()}
                                                                        </div>
                                                                        <div className="module-card__chart">
                                                                            <ChartistGraph type={'Bar'} data={availability.data} options={options}
                                                                                onClick = {(e)=>{
                                                                                    if (e.target.tagName === "line" 
                                                                                        && e.target.className.baseVal === "ct-bar"){
                                                                                            this.refs.standardDialog.show();
                                                                                    }
                                                                                }}
                                                                            />
                                                                            <ChartistGraph 
                                                                                type={'Line'}
                                                                                data={{labels:availability.data.labels, series:[availability.data.series[0]]}}
                                                                                options = {{
                                                                                    showArea: true,
                                                                                    fullWidth: true,
                                                                                    chartPadding: {
                                                                                        top: 5,
                                                                                        right: 30,
                                                                                        bottom: 30,
                                                                                        left: 43
                                                                                    },
                                                                                    height:"50px",
                                                                                    axisX: {offset: 0, showLabel: false, showGrid: false},
                                                                                    axisY: {offset: 20, showLabel: false, showGrid: false},
                                                                                    classNames: {series: 'area-series', label: 'area-label',}
                                                                                }}
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                    </MUI.CardText>
                                                </MUI.Card>
                                            )
                                        })
                                    }
                                    <MUI.Dialog
                                        ref="standardDialog"
                                        title="Details"
                                        actions={standardActions}
                                        autoDetectWindowHeight={true} 
                                        autoScrollBodyContent={true}
                                    >
                                        <div style={{height:"400px"}}>
                                            <div className="module-card__label--red">{this.props.logInfo.date.toString()}</div>
                                            <div className="module-card__log-info--error">
                                                <div className="module-card__log-info__section">
                                                    Remote Address: <span className="font-weight-400">{this.props.logInfo.headers.remoteAddress}</span>
                                                </div>
                                                <div className="module-card__log-info__section">
                                                    Request URL: <span className="font-weight-400">{this.props.logInfo.headers.requestURL}</span>
                                                </div>
                                                <div className="module-card__log-info__section">
                                                    Request Method: <span className="font-weight-400">{this.props.logInfo.headers.requestMethod}</span>
                                                </div>
                                                <div className="module-card__log-info__section">
                                                    Status Code: <span className="module-card__log-info__code-error font-weight-400">{this.props.logInfo.headers.statusCode}</span>
                                                </div>
                                                <div dangerouslySetInnerHTML={{"__html":this.props.logInfo.response}}/>
                                            </div>
                                            
                                            <div className="module-card__label--green">{this.props.logInfo.date.toString()}</div>
                                            <div className="module-card__log-info--success">
                                                <div className="module-card__log-info__section">
                                                    Remote Address: <span className="font-weight-400">{this.props.logInfo.headers.remoteAddress}</span>
                                                </div>
                                                <div className="module-card__log-info__section">
                                                    Request URL: <span className="font-weight-400">{this.props.logInfo.headers.requestURL}</span>
                                                </div>
                                                <div className="module-card__log-info__section">
                                                    Request Method: <span className="font-weight-400">{this.props.logInfo.headers.requestMethod}</span>
                                                </div>
                                                <div className="module-card__log-info__section">
                                                    Status Code: <span className="module-card__log-info__code-ok font-weight-400">200</span>
                                                </div>
                                            </div>
                                        </div>
                                    </MUI.Dialog>
                                </MUI.CardText>
                            </MUI.Card>
                            );
                        })
                    }
                })()
            }
            </div>
        );
    }
}

ModuleCard.defaultProps = {
    "logInfo": {
        date: new Date(),
        headers:{
            remoteAddress:"163.231.20.42:80",
            requestURL:"http://c111wvf.int.westgroup.com:8899",
            requestMethod:"GET",
            statusCode:500
        },
        response: "HTTP Error 500.19 - Internal Server Error\
        <br/>Description: The requested page cannot be accessed because the related configuration data for the page is invalid.\
        <br/>Module: StaticCompressionModule\
        <br/>Notification: MapRequestHandler\
        <br/>Handler: StaticFile\
        <br/>Error Code: 0x800700c1\
        <br/>Requested URL: http://localhost/\
        <br/>Physical Path: C:\\inetpub\\wwwroot\
        <br/>Logon Method: Anonymous\
        <br/>Logon User: Anonymous"
    }
};