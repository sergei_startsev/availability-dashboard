import * as React from "react";
import {RouteHandler, Link} from "react-router";
import * as MUI from "material-ui";

import {EnvironmentStatus, Status} from "./environment-status";
/**
 * ProductOverviewCard
 */
export class ProductOverviewCard extends React.Component<any, any>{
    private getRandom(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min + 1) + min);;
    }
    
    private getFakeValue(status: Status):string{
        switch (status) {
            case Status.Alive:
                return this.getRandom(98,100)+"%";
                
            case Status.PartiallyAlive:
                return this.getRandom(70,97)+"%";
                
            case Status.Broken:
                return this.getRandom(0,70)+"%";
                
            case Status.NoData:
                return "-";
                
            default:
                return "-";
        }
    }
    
    render() {
        //TODO: remove temporary data
        let ciStatus = this.getRandom(Status.Alive, Status.NoData);
        let demoStatus = this.getRandom(Status.Alive, Status.NoData);
        let qedStatus = this.getRandom(Status.Alive, Status.NoData);
        let hotprodStatus = this.getRandom(Status.Alive, Status.NoData);
        let prodStatus = this.getRandom(Status.Alive, Status.NoData);
        let classes = {
            0 : "product-overview-card__env-table__cell__value--alive",
            1 : "product-overview-card__env-table__cell__value--partially-alive",
            2 : "product-overview-card__env-table__cell__value--broken",
            3 : "product-overview-card__env-table__cell__value--no-data"
        };
        
        return (
            <MUI.Card className="product-overview-card card" initiallyExpanded={true}>
                <MUI.CardHeader
                    title={
                        <span className="product-overview-card__title">
                            {this.props.name}
                        </span>
                    }
                    subtitle={
                        <span className="product-overview-card__subtitle">
                            <EnvironmentStatus isFlat={true} status={ciStatus}/>
                            <EnvironmentStatus isFlat={true} status={demoStatus}/>
                            <EnvironmentStatus isFlat={true} status={qedStatus}/>
                            <EnvironmentStatus isFlat={true} status={hotprodStatus}/>
                            <EnvironmentStatus isFlat={true} status={prodStatus}/>
                        </span>
                    }
                    avatar={
                        <MUI.Avatar 
                            icon={
                                    <MUI.FontIcon className="material-icons">cloud_queue</MUI.FontIcon>
                            }
                            color={MUI.Styles.Colors.wight}
                            backgroundColor={MUI.Styles.Colors.grey300}
                        />
                    }
                    showExpandableButton={true}
                    className="product-brief-card__card-header">
                </MUI.CardHeader>
                <MUI.CardText expandable={true}>
                    <table className="product-overview-card__env-table">
                        <tr>
                            <td className="product-overview-card__env-table__cell font-weight-400 text-center">
                                <span>CI</span>
                            </td>
                            <td className="product-overview-card__env-table__cell font-weight-400 text-center">
                                <span>DEMO</span>
                            </td>
                            <td className="product-overview-card__env-table__cell font-weight-400 text-center">
                                <span>QED</span>
                            </td>
                            <td className="product-overview-card__env-table__cell font-weight-400 text-center">
                                <span className="product-overview-card__env-table__hotprod-column">
                                    <MUI.FontIcon 
                                        className="material-icons product-overview-card__env-table__hotprod-icon">
                                            whatshot
                                    </MUI.FontIcon>
                                    PROD 
                                </span>
                            </td>
                            <td className="product-overview-card__env-table__cell font-weight-400 text-center">
                                <span>PROD</span>
                            </td>
                        </tr>
                        <tr>
                            <td className="product-overview-card__env-table__cell text-center">
                                <EnvironmentStatus status={ciStatus}/>
                            </td>
                            <td className="product-overview-card__env-table__cell text-center">
                                <EnvironmentStatus status={demoStatus}/>
                            </td>
                            <td className="product-overview-card__env-table__cell text-center">
                                <EnvironmentStatus status={qedStatus}/>
                            </td>
                            <td className="product-overview-card__env-table__cell text-center">
                                <EnvironmentStatus status={hotprodStatus}/>
                            </td>
                            <td className="product-overview-card__env-table__cell text-center">
                                <EnvironmentStatus status={prodStatus}/>
                            </td>
                        </tr>
                        <tr>
                            <td className="product-overview-card__env-table__cell text-center">
                                <span className={classes[ciStatus]}>{this.getFakeValue(ciStatus)}</span>
                            </td>
                            <td className="product-overview-card__env-table__cell text-center">
                                <span className={classes[demoStatus]}>{this.getFakeValue(demoStatus)}</span>
                            </td>
                            <td className="product-overview-card__env-table__cell text-center">
                                <span className={classes[qedStatus]}>{this.getFakeValue(qedStatus)}</span>
                            </td>
                            <td className="product-overview-card__env-table__cell text-center">
                                <span className={classes[hotprodStatus]}>{this.getFakeValue(hotprodStatus)}</span>
                            </td>
                            <td className="product-overview-card__env-table__cell text-center">
                                <span className={classes[prodStatus]}>{this.getFakeValue(prodStatus)}</span>
                            </td>
                        </tr>
                    </table>
                    <div className="product-brief-card__timestamp">
                        09/07/2015 11:23:44 am
                    </div>
                    <div className="product-brief-card__more-info">
                        <Link to="products" className="app-bar__links__item">
                            <MUI.IconButton
                                tooltipPosition="top-center"
                                tooltip="More info"
                                style={{margin:"0"}}
                                iconStyle={{color: "#ff8000"}}>
                                <MUI.FontIcon className="material-icons">info</MUI.FontIcon>
                            </MUI.IconButton>
                        </Link>
                    </div>
                </MUI.CardText>
            </MUI.Card>
        );
    }
}