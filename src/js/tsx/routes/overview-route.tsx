import * as React from "react";
import * as MUI from "material-ui";
import {Link} from "react-router";

import {Row} from "../row";
import {Column} from "../column";
import {ProductOverviewCard} from "../product-overview-card";
import {CardHeader} from "../Components/CardHeader";
import {CardHeaderToolbar} from "../Components/CardHeaderToolbar";
import {CardFooter} from "../Components/CardFooter";
import {Label} from "../components/Label";
import {EcosystemCard} from "../widgets/EcosystemCard";
import {EnvironmentStatus, Status} from "../environment-status";

let ThemeManager = new MUI.Styles.ThemeManager();

export class OverviewRoute extends React.Component<void, void> {
    static childContextTypes = {
        muiTheme: React.PropTypes.object
    }
    getChildContext() {
        return {
            muiTheme: ThemeManager.getCurrentTheme()
        };
    }
    
    private getRandom(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min + 1) + min);;
    }
    
    private getFakeValue(status: Status):string{
        switch (status) {
            case Status.Alive:
                return this.getRandom(98,100).toString();
                
            case Status.PartiallyAlive:
                return this.getRandom(70,97).toString();
                
            case Status.Broken:
                return this.getRandom(0,70).toString();
                
            case Status.NoData:
                return "-";
                
            default:
                return "-";
        }
    }
    
    render() {
        let productFamiliesNames = ["WLN", "WLNTax", "Carswell", "TNPCarswell", "Cayman", "ANALYTICS", "Weblinks", "WestKM", "LegalServices", "Drafting" , "WLNSSL", "WLNClient"]
        
        return (
            <div>
                <Row>
                    <h2>Cobalt Ecosystem</h2>
                </Row>
                <Row>
                    <div className="toolbar">
                        <div className="clearfix">
                            <div className="toolbar__switcher">
                                <MUI.Toggle
                                    name="expander"
                                    label="Expand cards"
                                    labelPosition="right"
                                />
                            </div>
                            <div className="toolbar__switcher">
                                <MUI.Toggle
                                    name="expander"
                                    label="Real time"
                                    labelPosition="right"
                                    defaultToggled={true}
                                />
                            </div>
                            <div className="toolbar__search">
                                <MUI.TextField
                                    hintText="Example: WLN"
                                    floatingLabelText="Search by families"
                                    floatingLabelStyle={{color: "#ff8000"}} 
                                />
                            </div>
                        </div>
                    </div>
                    <div className="toolbar">
                        <div className="toolbar__date clearfix">
                            <div className="toolbar__datepicker-label left">
                                <MUI.FontIcon className="material-icons toolbar__datepicker-icon">schedule</MUI.FontIcon>Date from: 
                            </div>
                            <div className="toolbar__datepicker">
                                    <MUI.DatePicker hintText="Portrait Dialog"  
                                        defaultDate={new Date()}
                                        textFieldStyle={{width:"100px"}}
                                        autoOk={true}
                                    />
                            </div>
                            <div className="toolbar__datepicker-label left" >
                                <MUI.FontIcon className="material-icons toolbar__datepicker-icon">schedule</MUI.FontIcon>Date to: 
                            </div>
                            <div className="toolbar__datepicker">
                                    <MUI.DatePicker hintText="Portrait Dialog" 
                                        defaultDate={new Date()}
                                        textFieldStyle={{width:"100px"}}
                                        autoOk={true}
                                    />
                            </div>
                        </div>
                    </div>
                </Row>
                <Row>
                    {productFamiliesNames.map((family, index)=>{
                        let classNames = "large-3";
                        if (index == productFamiliesNames.length - 1) {
                            classNames += " end";
                        }
                        
                         //TODO: remove temporary data
                        let ciStatus = this.getRandom(Status.Alive, Status.NoData);
                        let demoStatus = this.getRandom(Status.Alive, Status.NoData);
                        let qedStatus = this.getRandom(Status.Alive, Status.NoData);
                        let hotprodStatus = this.getRandom(Status.Alive, Status.NoData);
                        let prodStatus = this.getRandom(Status.Alive, Status.NoData);
                        
                        let cardClassName = "w-card w-card_borders";
                        cardClassName += (prodStatus==Status.Broken)?" w-card_alert w-card_alert-borders":"";
                        
                        return (
                            <Column key={index} className={classNames}>
                                <EcosystemCard
                                    className={cardClassName}
                                    header={
                                        <CardHeader
                                            title = {family}
                                            className="w-card__header w-card__header_fullwidth"
                                            primaryToolbar = {
                                                <CardHeaderToolbar 
                                                    showCloseButton={true} 
                                                    showCollapseButton={true}
                                                    showFocusButton={true}
                                                />
                                            }
                                        />
                                    }
                                    footer = {
                                        <CardFooter
                                            className="w-card__footer w-card__footer_fullwidth"
                                            leftPanel = {
                                                <Label value = "11/17/2015 12:23:44am" 
                                                        className="c-label c-label_light c-label_inherit" />
                                            }
                                            rightPanel = {
                                                <Link to="products">
                                                    <i className="pe-7s-more pe-lg pe-va"/>
                                                </Link>
                                            }
                                        />
                                    }
                                    /*In real life this data will be passed from a datasource */
                                    data = {{
                                        statistics:{ 
                                            ci: {status: ciStatus, availability: this.getFakeValue(ciStatus)},
                                            demo: {status: demoStatus, availability: this.getFakeValue(demoStatus)},
                                            qed: {status: qedStatus, availability: this.getFakeValue(qedStatus)},
                                            hotprod: {status: hotprodStatus, availability: this.getFakeValue(hotprodStatus)},
                                            prod: {status: prodStatus, availability: this.getFakeValue(prodStatus)}, 
                                        }
                                    }}
                                />
                            </Column>
                        );
                    })}
                </Row>
            </div>
        );
    }
}