import * as React from "react";
import * as MUI from "material-ui";
import * as ChartistGraph from "react-chartist";

import {Row} from "../row";
import {Column} from "../column";
import {Card} from "../widgets/Card";
import {CardHeader} from "../Components/CardHeader";
import {CardHeaderToolbar} from "../Components/CardHeaderToolbar";
import {CardFooter} from "../Components/CardFooter";
import {Label} from "../components/Label";
import {EnvironmentStatus, Status} from "../environment-status";
import {BriefModulesStats} from "../brief-modules-stats";
import {ProductFamilyCard} from "../widgets/ProductFamilyCard";
import {EcosystemCard} from "../widgets/EcosystemCard";

export class DashboardRoute extends React.Component<void, void>{
    static childContextTypes = {
        muiTheme: React.PropTypes.object
    }
    getChildContext() {
        return {
            muiTheme: MUI.Styles.ThemeManager().getCurrentTheme()
        };
    }
    private getRandom(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min + 1) + min);;
    }
    
    private getFakeValue(status: Status):string{
        switch (status) {
            case Status.Alive:
                return this.getRandom(98,100).toString();
                
            case Status.PartiallyAlive:
                return this.getRandom(70,97).toString();
                
            case Status.Broken:
                return this.getRandom(0,70).toString();
                
            case Status.NoData:
                return "-";
                
            default:
                return "-";
        }
    }
    render() {
        let data = {
            labels: ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B9', 'B10'],
            series: [
                [5, 3, 4, 8, 6, 2, 7, 4, 6, 3]
            ]
        }
        let failedData = {
            labels: ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B9', 'B10'],
            series: [
                [4, 5, 4, 6, 5, 0, 6, 3, 5, 0],
                [0, 0, 0, 0, 0, 1, 0, 0, 0, 5]
            ]
        }
        let options = {
            height: "50px",
            stackBars: true,
            chartPadding: {
                top: 0,
                right: 0,
                bottom: 0,
                left: 0
            },
            axisX: {
                offset: 0,
                showLabel: false,
                showGrid: false
            },
            axisY: {
                offset: 0,
                showLabel: false,
                showGrid: false
            },
            classNames: {
                chart: 'chart-builds',
                series: 'build-series',
                bar: 'build',
            }
        };
        
        //TODO: remove temporary data
        let ciStatus = this.getRandom(Status.Alive, Status.NoData);
        let demoStatus = this.getRandom(Status.Alive, Status.NoData);
        let qedStatus = this.getRandom(Status.Alive, Status.NoData);
        let hotprodStatus = this.getRandom(Status.Alive, Status.NoData);
        let prodStatus = this.getRandom(Status.Alive, Status.NoData);
        
        let stats = {"availableModulesCount":22,"brokenModulesCount":10,"undefinedModulesCount":9,"availability24h":[98,80,100,97,85,85,93,85,91,96,80,82,90,96,84,90,90,82,92,99,82,99,82,92]};
        
        return (
            <div>
                <Row>
                    <h2>Custom Dashboard</h2>
                </Row>
                <Row>
                    <Column className="large-4">
                        <EcosystemCard
                            header={
                                <CardHeader
                                    title = {
                                        <span>
                                            <Label value = "WLN" className="c-label c-label_light c-label_inherit" />
                                            <Label value = "ECOSYSTEM" className="c-label c-label_primary c-label_inherit" />
                                        </span>
                                    }
                                    primaryToolbar = {
                                        <CardHeaderToolbar 
                                            showCloseButton={true} 
                                            showCollapseButton={true}
                                            showSettingsButton={true}
                                            showFocusButton={true}
                                        />
                                    }
                                />
                            }
                            footer = {
                                <CardFooter
                                    className="w-card__footer w-card__footer_fullwidth"
                                    leftPanel = {
                                        <Label value = "11/17/2015 12:23:44am" 
                                                className="c-label c-label_light c-label_inherit" />
                                    }
                                    rightPanel = {
                                        <i className="pe-7s-more pe-lg pe-va"/>
                                    }
                                />
                            }
                            /*In real life this data will be passed from a datasource */
                            data = {{
                                statistics:{ 
                                    ci: {status: ciStatus, availability: this.getFakeValue(ciStatus)},
                                    demo: {status: demoStatus, availability: this.getFakeValue(demoStatus)},
                                    qed: {status: qedStatus, availability: this.getFakeValue(qedStatus)},
                                    hotprod: {status: hotprodStatus, availability: this.getFakeValue(hotprodStatus)},
                                    prod: {status: prodStatus, availability: this.getFakeValue(prodStatus)}, 
                                }
                            }}
                        />
                        <ProductFamilyCard
                            header={
                                <CardHeader
                                    title = {
                                        <span>
                                            <Label value = "WLN" className="c-label c-label_light c-label_inherit" />
                                            <Label value = "CI" className="c-label c-label_primary c-label_inherit" />
                                        </span>
                                    }
                                    primaryToolbar = {
                                        <CardHeaderToolbar 
                                            showCloseButton={true} 
                                            showCollapseButton={true}
                                            showSettingsButton={true}
                                            showFocusButton={true}
                                        />
                                    }
                                />
                            }
                            footer = {
                                <CardFooter
                                    className="w-card__footer w-card__footer_fullwidth"
                                    leftPanel = {
                                        <Label value = "11/17/2015 12:23:44am" 
                                                className="c-label c-label_light c-label_inherit" />
                                    }
                                    rightPanel = {
                                        <i className="pe-7s-more pe-lg pe-va"/>
                                    }
                                />
                            }
                            productFamily = {{statistics:stats}}
                        />
                        
                    </Column>
                    <Column className="large-8">
                        <Card
                            header={
                                <CardHeader
                                    title = {
                                        <span><i className="pe-7s-box2 pe-lg pe-va"/> TFS builds </span>
                                    }
                                    primaryToolbar = {
                                        <CardHeaderToolbar 
                                            showCloseButton={true} 
                                            showCollapseButton={true}
                                            showSettingsButton={true}
                                            showFocusButton={true}
                                        />
                                    }
                                    secondaryToolbar = {
                                        <div>
                                            <Label value = "COBALT WEBSITE" className="c-label c-label_primary" />
                                            <Label value = "CI" />
                                            <Label value = "FAILED" className="c-label c-label_alert" />
                                        </div>
                                    }
                                />
                            }
                            footer = {
                                <CardFooter
                                    className="w-card__footer w-card__footer_fullwidth"
                                    leftPanel = {
                                        <div>
                                            <span> Builds summary: </span>
                                            <Label value = "4" className="c-label c-label_success c-label_round c-label_inherit" />
                                            <span> Succeeded </span>
                                            <Label value = "1" className="c-label c-label_alert c-label_round c-label_inherit" />
                                            <span> Failed </span>
                                        </div>
                                    }
                                />
                            }
                        >
                            <table className="table table-striped table-fw-widget table-hover">
                                <thead>
                                    <tr>
                                    <th width="25%">Build Name</th>
                                    <th width="25%">User</th>
                                    <th>Commit</th>
                                    <th width="20%"></th>
                                    </tr>
                                </thead>
                                <tbody className="no-border-x">
                                    <tr>
                                        <td>
                                            <div>Website</div>
                                            <Label value="29.5.51" className="c-label c-label_success c-label_inherit"/>
                                            <Label value="08/07/2015" className="c-label c-label_light c-label_inherit"/>
                                        </td>
                                        <td className="user-avatar"> 
                                            <img src="http://www.fillmurray.com/g/100/100"/>Claire Sassu
                                        </td>
                                        <td>Initial commit</td>
                                        <td>
                                            <ChartistGraph type={'Bar'} data={data} options={options}/>
                                        </td>
                                    </tr>
                                    <tr className="tr tr_alert">
                                        <td>
                                            <div>WLNTaxWebsite</div>
                                            <Label value="29.5.25" className="c-label c-label_alert c-label_inherit"/>
                                            <Label value="08/07/2015" className="c-label c-label_light c-label_inherit"/>
                                        </td>
                                        <td className="user-avatar">
                                            <img src="http://www.fillmurray.com/g/100/101"/>Joel King
                                        </td>
                                        <td>Main structure markup</td>
                                        <td>
                                            <ChartistGraph type={'Bar'} data={failedData} options={options}/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>FAWebsite</div>
                                            <Label value="29.5.26" className="c-label c-label_success c-label_inherit"/>
                                            <Label value="08/07/2015" className="c-label c-label_light c-label_inherit"/>
                                        </td>
                                        <td className="user-avatar">
                                            <img src="http://www.fillmurray.com/g/100/102"/>Maggie Jackson
                                        </td>
                                        <td>
                                            Left sidebar adjusments
                                        </td>
                                        <td>
                                            <ChartistGraph type={'Bar'} data={data} options={options}/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>AnalyticsWebsite</div>
                                            <Label value="29.5.26" className="c-label c-label_success c-label_inherit"/>
                                            <Label value="08/07/2015" className="c-label c-label_light c-label_inherit"/>
                                        </td>
                                        <td className="user-avatar">
                                            <img src="http://www.fillmurray.com/g/100/103"/>Mike Bolthort
                                        </td>
                                        <td>Topbar dropdown style</td>
                                        <td>
                                            <ChartistGraph type={'Bar'} data={data} options={options}/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>NewsWebsite</div>
                                            <Label value="29.5.26" className="c-label c-label_success c-label_inherit"/>
                                            <Label value="08/07/2015" className="c-label c-label_light c-label_inherit"/>
                                        </td>
                                        <td className="user-avatar">
                                            <img src="http://www.fillmurray.com/g/100/107"/>Mike Bolthort
                                        </td>
                                        <td>Topbar dropdown style</td>
                                        <td>
                                            <ChartistGraph type={'Bar'} data={data} options={options}/>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </Card>
                    </Column>
                  </Row>
            </div>
        );
    }
}