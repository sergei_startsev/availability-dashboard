import * as React from "react";
import * as MUI from "material-ui";

import {Row} from "../row";
import {ProductBriefCardsListWrapper} from "../product-brief-cards-list-wrapper";

let ThemeManager = new MUI.Styles.ThemeManager();

export class ProductsRoute extends React.Component<void, void> {
    static childContextTypes = {
        muiTheme: React.PropTypes.object
    }
    getChildContext() {
        return {
            muiTheme: ThemeManager.getCurrentTheme()
        };
    }
    render() {
        return (
            <div>
                <Row>
                    <h2>Product Families</h2>
                </Row>
                <Row>
                    <div className="toolbar clearfix">
                        <div className="toolbar__switcher">
                            <MUI.Toggle
                                name="expander"
                                label="Expand cards"
                                labelPosition="right"
                            />
                        </div>
                        <div className="toolbar__switcher">
                            <MUI.Toggle
                                name="expander"
                                label="Real time"
                                labelPosition="right"
                                defaultToggled={true}
                            />
                        </div>
                        <div className="toolbar__search">
                            <MUI.TextField
                                hintText="Example: WLN"
                                floatingLabelText="Search by a family"
                                floatingLabelStyle={{color: "#ff8000"}} 
                            />
                        </div>
                        <div className="toolbar__filter">
                            <MUI.DropDownMenu 
                                menuItems={[
                                    { payload: '1', text: 'None' },
                                    { payload: '2', text: 'CI' },
                                    { payload: '3', text: 'DEMO' },
                                    { payload: '4', text: 'QED' },
                                    { payload: '5', text: 'HOTPROD' },
                                    { payload: '5', text: 'PROD' }
                                ]} 
                            />
                        </div>
                    </div>
                    <div className="toolbar">
                        <div className="toolbar__date clearfix">
                            <div className="toolbar__datepicker-label left">
                                <MUI.FontIcon className="material-icons toolbar__datepicker-icon">schedule</MUI.FontIcon>Date from: 
                            </div>
                            <div className="toolbar__datepicker">
                                    <MUI.DatePicker hintText="Portrait Dialog"  
                                        defaultDate={new Date()}
                                        textFieldStyle={{width:"100px"}}
                                        autoOk={true}
                                    />
                            </div>
                            <div className="toolbar__datepicker-label left" >
                                <MUI.FontIcon className="material-icons toolbar__datepicker-icon">schedule</MUI.FontIcon>Date to: 
                            </div>
                            <div className="toolbar__datepicker">
                                    <MUI.DatePicker hintText="Portrait Dialog" 
                                        defaultDate={new Date()}
                                        textFieldStyle={{width:"100px"}}
                                        autoOk={true}
                                    />
                            </div>
                        </div>
                    </div>
                </Row>
                <div className="row">
                    <h3 className="section-title">WLN</h3>
                </div>
                
                <ProductBriefCardsListWrapper/>
                
            </div>
        );
    }
}