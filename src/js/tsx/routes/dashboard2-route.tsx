import * as React from "react";
import * as MUI from "material-ui";

import {Row} from "../row";
import {Column} from "../column";
import {Card} from "../widgets/Card";
import {CardHeader} from "../Components/CardHeader";
import {CardHeaderToolbar} from "../Components/CardHeaderToolbar";
import {CardFooter} from "../Components/CardFooter";
import {Label} from "../components/Label";

export class Dashboard2Route extends React.Component<void, void>{
    static childContextTypes = {
        muiTheme: React.PropTypes.object
    }
    getChildContext() {
        return {
            muiTheme: MUI.Styles.ThemeManager().getCurrentTheme()
        };
    }
    render() {
        return (
            <div>
                <Row>
                    <h2>Custom Dashboard</h2>
                </Row>
                <Row>
                    <Column className="large-4">
                        <Card
                            header={
                                <CardHeader
                                    title = "Default Card"
                                />
                            } 
                        >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur recusandae a mollitia magni in.
                            <br/>
                            Velit numquam possimus eum delectus maiores dicta perspiciatis quae aperiam dolores facere, doloribus non, iusto, sit.
                        </Card>
                    </Column>
                    <Column className="large-4">
                        <Card
                            header={
                                <CardHeader
                                    title = "Card with tools"
                                    primaryToolbar = {
                                        <CardHeaderToolbar 
                                            showCloseButton={true} 
                                            showCollapseButton={true}
                                            showSettingsButton={true}
                                            showFocusButton={true}
                                        />
                                    }
                                />
                            } 
                        >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur recusandae a mollitia magni in.
                            <br/>
                            Velit numquam possimus eum delectus maiores dicta perspiciatis quae aperiam dolores facere, doloribus non, iusto, sit.
                        </Card>
                    </Column>
                    <Column className="large-4">
                        <Card
                            className="w-card w-card_borders"
                            header={
                                <CardHeader
                                    title = "Card with borders"
                                />
                            } 
                        >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur recusandae a mollitia magni in.
                            <br/>
                            Velit numquam possimus eum delectus maiores dicta perspiciatis quae aperiam dolores facere, doloribus non, iusto, sit.
                        </Card>
                    </Column>
                    <Column className="large-4">
                        <Card
                            header={
                                <CardHeader
                                    className="w-card__header w-card__header_fullwidth"
                                    title = {<span><i className="pe-7s-graph pe-2x pe-va pe-spin"/> Card heading full-width</span>}
                                />
                            } 
                        >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur recusandae a mollitia magni in.
                            <br/>
                            Velit numquam possimus eum delectus maiores dicta perspiciatis quae aperiam dolores facere, doloribus non, iusto, sit.
                        </Card>
                    </Column>
                    <Column className="large-4">
                        <Card
                            className="w-card w-card_transparent w-card_borders"
                            header={
                                <CardHeader
                                    title = "Transparent"
                                    primaryToolbar = {
                                        <CardHeaderToolbar 
                                            showCloseButton={true} 
                                            showCollapseButton={true}
                                        />
                                    }
                                />
                            } 
                        >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur recusandae a mollitia magni in.
                            <br/>
                            Velit numquam possimus eum delectus maiores dicta perspiciatis quae aperiam dolores facere, doloribus non, iusto, sit.
                        </Card>
                    </Column>
                    <Column className="large-4">
                        <Card
                            header={
                                <CardHeader
                                    className="w-card__header w-card__header_no-border"
                                    title = {<i>Card without divider</i>}
                                />
                            } 
                        >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur recusandae a mollitia magni in.
                            <br/>
                            Velit numquam possimus eum delectus maiores dicta perspiciatis quae aperiam dolores facere, doloribus non, iusto, sit.
                        </Card>
                    </Column>
                </Row>
                <Row>
                    <Column className="large-4">
                        <Card
                            header={
                                <CardHeader
                                    className="w-card__header w-card__header_primary"
                                    title = "Primary color"
                                    primaryToolbar = {
                                        <CardHeaderToolbar 
                                            showCloseButton={true} 
                                            showCollapseButton={true}
                                            showSettingsButton={true}
                                            showFocusButton={true}
                                        />
                                    }
                                />
                            } 
                        >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur recusandae a mollitia magni in.
                            <br/>
                            Velit numquam possimus eum delectus maiores dicta perspiciatis quae aperiam dolores facere, doloribus non, iusto, sit.
                        </Card>
                    </Column>
                    <Column className="large-4">
                        <Card
                            header={
                                <CardHeader
                                    className="w-card__header w-card__header_secondary"
                                    title = "Secondary color"
                                    primaryToolbar = {
                                        <CardHeaderToolbar 
                                            showCloseButton={true} 
                                            showCollapseButton={true}
                                            showSettingsButton={true}
                                            showFocusButton={true}
                                        />
                                    }
                                />
                            } 
                        >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur recusandae a mollitia magni in.
                            <br/>
                            Velit numquam possimus eum delectus maiores dicta perspiciatis quae aperiam dolores facere, doloribus non, iusto, sit.
                        </Card>
                    </Column>
                    <Column className="large-4">
                        <Card
                            header={
                                <CardHeader
                                    className="w-card__header w-card__header_dark"
                                    title = "Dark Card"
                                    primaryToolbar = {
                                        <CardHeaderToolbar 
                                            showCloseButton={true} 
                                            showCollapseButton={true}
                                            showSettingsButton={true}
                                            showFocusButton={true}
                                        />
                                    }
                                />
                            } 
                        >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur recusandae a mollitia magni in.
                            <br/>
                            Velit numquam possimus eum delectus maiores dicta perspiciatis quae aperiam dolores facere, doloribus non, iusto, sit.
                        </Card>
                    </Column>
                    <Column className="large-4">
                        <Card
                            className="w-card w-card_primary"
                            header={
                                <CardHeader
                                    className="w-card__header w-card__header_full-secondary"
                                    title = "Primary color"
                                    primaryToolbar = {
                                        <CardHeaderToolbar 
                                            showCloseButton={true} 
                                            showCollapseButton={true}
                                            showSettingsButton={true}
                                            showFocusButton={true}
                                        />
                                    }
                                />
                            } 
                        >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur recusandae a mollitia magni in.
                            <br/>
                            Velit numquam possimus eum delectus maiores dicta perspiciatis quae aperiam dolores facere, doloribus non, iusto, sit.
                        </Card>
                    </Column>
                    <Column className="large-4">
                        <Card
                            className="w-card w-card_secondary"
                            header={
                                <CardHeader
                                    className="w-card__header w-card__header_full-secondary"
                                    title = "Secondary color"
                                    primaryToolbar = {
                                        <CardHeaderToolbar 
                                            showCloseButton={true} 
                                            showCollapseButton={true}
                                            showSettingsButton={true}
                                            showFocusButton={true}
                                        />
                                    }
                                />
                            } 
                        >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur recusandae a mollitia magni in.
                            <br/>
                            Velit numquam possimus eum delectus maiores dicta perspiciatis quae aperiam dolores facere, doloribus non, iusto, sit.
                        </Card>
                    </Column>
                    <Column className="large-4">
                        <Card
                            className="w-card w-card_dark"
                            header={
                                <CardHeader
                                    className="w-card__header w-card__header_full-dark"
                                    title = "Dark Card"
                                    primaryToolbar = {
                                        <CardHeaderToolbar 
                                            showCloseButton={true} 
                                            showCollapseButton={true}
                                            showSettingsButton={true}
                                            showFocusButton={true}
                                        />
                                    }
                                />
                            } 
                        >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur recusandae a mollitia magni in.
                            <br/>
                            Velit numquam possimus eum delectus maiores dicta perspiciatis quae aperiam dolores facere, doloribus non, iusto, sit.
                        </Card>
                    </Column>
                </Row>
                <Row>
                    <Column className="large-4">
                        <Card
                            header={
                                <CardHeader
                                    title = "Card with labels"
                                    secondaryToolbar = {
                                        <div>
                                            <Label value = "WLN" className="c-label c-label_secondary" />
                                            <Label value = "NO DATA" />
                                        </div>
                                    }
                                />
                            } 
                        >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur recusandae a mollitia magni in.
                            <br/>
                            Velit numquam possimus eum delectus maiores dicta perspiciatis quae aperiam dolores facere, doloribus non, iusto, sit.
                        </Card>
                    </Column>
                    <Column className="large-4">
                        <Card
                            header={
                                <CardHeader
                                    title = "Badges"
                                    secondaryToolbar = {
                                        <div>
                                            <Label 
                                                value = "7" className="c-label c-label_success c-label_round" />
                                            <Label value = "3" className="c-label c-label_alert c-label_round" />
                                        </div>
                                    }
                                />
                            } 
                        >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur recusandae a mollitia magni in.
                            <br/>
                            Velit numquam possimus eum delectus maiores dicta perspiciatis quae aperiam dolores facere, doloribus non, iusto, sit.
                        </Card>
                    </Column>
                    <Column className="large-4">
                        <Card
                            header={
                                <CardHeader
                                    title = "Trend Labels"
                                    secondaryToolbar = {
                                        <div>
                                            <Label value = "⇧ 9 %" className="c-label c-label_success" />
                                            <Label value = "⇩ 2 %" className="c-label c-label_alert" />
                                        </div>
                                    }
                                />
                            } 
                        >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur recusandae a mollitia magni in.
                            <br/>
                            Velit numquam possimus eum delectus maiores dicta perspiciatis quae aperiam dolores facere, doloribus non, iusto, sit.
                        </Card>
                    </Column>
                    <Column className="large-4">
                        <Card
                            className="w-card w-card_secondary"
                            header={
                                <CardHeader
                                    title = "Trend Labels"
                                    className="w-card__header w-card__header_full-secondary"
                                    secondaryToolbar = {
                                        <div>
                                            <span style={{fontSize: ".9em", color: "#999"}}><i className="pe-7s-volume pe-lg pe-va"/> sound </span>
                                            <div style={{display: "inline-block", verticalAlign: "bottom"}}>
                                                <MUI.Toggle
                                                    name="toggleName"
                                                    value="toggleValue"
                                                />
                                            </div>
                                        </div>
                                    }
                                />
                            } 
                        >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur recusandae a mollitia magni in.
                            <br/>
                            Velit numquam possimus eum delectus maiores dicta perspiciatis quae aperiam dolores facere, doloribus non, iusto, sit.
                        </Card>
                    </Column>
                    <Column className="large-4">
                        <Card
                            header={
                                <CardHeader
                                    title = "Trend Labels"
                                    
                                />
                            }
                            footer = {
                                <CardFooter
                                    rightPanel = {
                                        <div>
                                            <Label value = "WLNTAX" className="c-label c-label_primary" />
                                            <Label value = "v.29.5.001"/>
                                        </div>
                                    }
                                />
                            }
                        >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur recusandae a mollitia magni in.
                            <br/>
                            Velit numquam possimus eum delectus maiores dicta perspiciatis quae aperiam dolores facere, doloribus non, iusto, sit.
                        </Card>
                    </Column>
                    <Column className="large-4">
                        <Card
                            header={
                                <CardHeader
                                    title = "Trend Labels"
                                    
                                />
                            }
                            footer = {
                                <CardFooter
                                    className="w-card__footer w-card__footer_no-border"
                                    leftPanel = {
                                        <div>
                                            <Label value = "WLNTAX" className="c-label c-label_primary" />
                                            <Label value = "v.29.5.001"/>
                                        </div>
                                    }
                                    rightPanel = {
                                        <div>
                                            <Label value = "8" className="c-label c-label_success c-label_round" />
                                            <Label value = "4" className="c-label c-label_alert c-label_round" />
                                        </div>
                                    }
                                />
                            }
                        >
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur recusandae a mollitia magni in.
                            <br/>
                            Velit numquam possimus eum delectus maiores dicta perspiciatis quae aperiam dolores facere, doloribus non, iusto, sit.
                        </Card>
                    </Column>
                </Row>
            </div>
        );
    }
}