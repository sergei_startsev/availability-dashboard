import * as React from "react";
import {TrendEnum} from "./tracked-value";

interface Props extends React.Props<any> {
    value: TrendEnum;
}

export class TrendArrow extends React.Component<Props, void>{
    render(){
        return (
            <span className="trend-arrow">
                {this.props.value==TrendEnum.Up ? <span className="trend-arrow-up">⇧</span> : <span className="trend-arrow-down">⇩</span>}
            </span>
        );
    }
}