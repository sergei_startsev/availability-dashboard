import * as React from "react";
import * as MUI from "material-ui";

import {TrackedValue} from "./tracked-value";

export class BriefModulesStats extends React.Component<any, any>{
    constructor(){
        this.props = {
            className: "brief-modules-stats-default"
        };
        super();
    }
    render(){
        return (
            <div className={this.props.className}>
                <div className="brief-modules-stats__title">
                    {this.props.title}
                </div>
                <TrackedValue 
                    value={this.props.percentageValue}
                    showPercentage = {true}
                    showTrend = {true}
                    className ="brief-modules-stats__percentageValue"
                />
                <div className="brief-modules-stats__value">
                    <span>{this.props.value}</span> {" module(s)"}
                </div>
            </div>
        );
    }
}