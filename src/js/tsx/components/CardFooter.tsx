import * as React from "react";

export class CardFooter extends React.Component<any, any>{

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={this.props.className}>
                <div className="clearfix">
                    <div className="w-card__footer-left-panel">
                        {this.props.leftPanel}
                    </div>
                    <div className="w-card__footer-right-panel">
                        {this.props.rightPanel}
                    </div>
                </div>
            </div>
        );
    }
}

CardFooter.defaultProps = {
    className: "w-card__footer"
}