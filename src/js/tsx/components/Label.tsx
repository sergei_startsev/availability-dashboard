import * as React from "react";

export class Label extends React.Component<any, any>{

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={this.props.className}>
                {this.props.value}
            </div>
        );
    }
}

Label.defaultProps = {
    className: "c-label c-label_default"
}