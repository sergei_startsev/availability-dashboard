import * as React from "react";

export class CardHeader extends React.Component<any, any>{

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={this.props.className}>
                <div className="clearfix">
                    <div className="w-card__header-title">
                        {this.props.title}
                    </div>
                    <div className="w-card__header-primary-toolbar">
                        {this.props.primaryToolbar}
                    </div>
                    <div className="w-card__header-secondary-toolbar">
                        {this.props.secondaryToolbar}
                    </div>
                </div>
            </div>
        );
    }
}

CardHeader.defaultProps = {
    title: "",
    className: "w-card__header"
}