import * as React from "react";

export class CardHeaderToolbar extends React.Component<any, any>{

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                {this.props.showFocusButton ? <i className="pe-7s-look pe-va f-large-8"></i> : null}
                {this.props.showSettingsButton ? <i className="pe-7s-edit pe-va f-large-8"></i> : null}
                {this.props.showExpandButton ? <i className="pe-7s-angle-down pe-va f-large-8"></i> : null}
                {this.props.showCollapseButton ? <i className="pe-7s-angle-up pe-va f-large-8"></i> : null}
                {this.props.showCloseButton ? <i className="pe-7s-close pe-va f-large-8"></i> : null}
            </div>
        )
    }
}