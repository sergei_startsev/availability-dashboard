import * as React from "react";
import * as MUI from "material-ui";
import {Link} from "react-router";

import {Row} from "./row";
import {Column} from "./column";
import {ProductBriefCard} from "./product-brief-card";
import {Card} from "./widgets/Card";
import {CardHeader} from "./Components/CardHeader";
import {CardHeaderToolbar} from "./Components/CardHeaderToolbar";
import {CardFooter} from "./Components/CardFooter";
import {Label} from "./components/Label";
import {ProductFamilyCard} from "./widgets/ProductFamilyCard";

//used MUI components
let ThemeManager = new MUI.Styles.ThemeManager();
//let Card = MUI.Card;

export class ProductBriefCardsList extends React.Component<any, any>{
    static childContextTypes = {
        muiTheme: React.PropTypes.object
    }
    
    constructor(){
        this.props = {"products": []};
        super();
    }
    
    getChildContext() {
        return {
            muiTheme: ThemeManager.getCurrentTheme()
        };
    }
    
    render(){
        return(
            <div>
                <Row>
                {this.props.products.map((product, index)=>{
                    let classNames = "large-4";
                    if (index == this.props.products.length - 1) {
                        classNames += " end";
                    }
                    return (
                        <Column key={index} className={classNames}>
                            <ProductFamilyCard
                            header={
                                <CardHeader
                                    title = {
                                        <span>
                                            <Label value = {product.productName} className="c-label c-label_light c-label_inherit" />
                                            <Label value = {product.env} className="c-label c-label_primary c-label_inherit" />
                                        </span>
                                    }
                                    primaryToolbar = {
                                        <CardHeaderToolbar 
                                            showCloseButton={true} 
                                            showCollapseButton={true}
                                            showSettingsButton={true}
                                            showFocusButton={true}
                                        />
                                    }
                                />
                            }
                            footer = {
                                <CardFooter
                                    className="w-card__footer w-card__footer_fullwidth"
                                    leftPanel = {
                                        <Label value = "11/17/2015 12:23:44am" 
                                                className="c-label c-label_light c-label_inherit" />
                                    }
                                    rightPanel = {
                                        <Link to="modules">
                                            <i className="pe-7s-more pe-lg pe-va"/>
                                        </Link>
                                    }
                                />
                            }
                            productFamily = {{statistics:product.statistics}}
                        />
                        </Column>
                    );
                })}
                </Row>
            </div>
        );
    }
}