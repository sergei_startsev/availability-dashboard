/// <reference path="../../typings/react/react.d.ts"/>

//import modules from node_modules
import * as React from "react";
import * as Router from "react-router";
import * as mui from "material-ui";
import * as injectTapEventPlugin from "react-tap-event-plugin";

import * as Test from "./core/events/test";
import {Routes} from "./routes";

export class App {

    public static Main(): void {
        //Needed for onTouchTap
        //Can go away when react 1.0 release
        //Check this repo:
        //https://github.com/zilverline/react-tap-event-plugin
        injectTapEventPlugin();
        
        Router.run(Routes, Router.HashLocation, (Root) => {
            React.render(React.createElement(Root), document.body);
        });
    }

}
