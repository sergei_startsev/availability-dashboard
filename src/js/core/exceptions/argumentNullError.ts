'use strict';

import * as Errors from './argumentError';

export class ArgumentNullError extends Errors.ArgumentError {
	constructor(public message?: string) {
		super(message);
		this.name = "ArgumentNullError";
	}
}