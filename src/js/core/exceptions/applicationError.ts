'use strict';

import * as Errors from "./baseError";

export class ApplicationError extends Errors.BaseError {
	constructor(public message?: string) {
		super(message);
		this.name = "ApplicationError";
	}
}