'use strict';

import * as Errors from "./applicationError"

export class StorageKeyNotFoundError extends Errors.ApplicationError {
	constructor(public message?: string) {
		super(message);
		this.name = "StorageKeyNotFoundError";
	}
}