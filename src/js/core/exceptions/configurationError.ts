'use strict';

import * as Errors from "./applicationError";

export class ConfigurationError extends Errors.ApplicationError {
	constructor(public message?: string) {
		super(message);
		this.name = "ConfigurationError";
	}
}