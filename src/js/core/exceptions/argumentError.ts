'use strict';

import * as Errors from "./applicationError"

export class ArgumentError extends Errors.ApplicationError {
	constructor(public message?: string) {
		super(message);
		this.name = "ArgumentError";
	}
}