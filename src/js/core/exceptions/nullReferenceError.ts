'use strict';

import * as Errors from "./applicationError";

export class NullReferenceError extends Errors.ApplicationError {
	constructor(public message?: string) {
		super(message);
		this.name = "NullReferenceError";
	}
}