import * as React from "react";
import {RouteHandler, Link} from "react-router";
import * as MUI from "material-ui";

//used MUI components
let ThemeManager = new MUI.Styles.ThemeManager();

export class AppTemplate extends React.Component<any, any>{
    static childContextTypes = {
        muiTheme: React.PropTypes.object
    }
    
    getChildContext() {
        return {
            muiTheme: ThemeManager.getCurrentTheme()
        };
    }
    
    render() {
        return (
            <div className="app-wrapper">
                <nav className="c-navbar c-navbar_default c-navbar_fixed-top">
                    <div className="c-navbar__container-fluid">
                        <div className="c-navbar__header">
                            <div className="page-title"><span>Availability Dashboard</span></div>
                        </div>
                        <a href="#" className="c-navbar__menu">
                            <i className="pe-7s-menu pe-va f-large-10"/>
                        </a>
                        <div>
                            <ul className="c-navbar__nav c-navbar__nav-right">
                                <li className="c-navbar__item">
                                    <a className="c-navbar__icon-item">
                                        <img className="c-navbar__user-avatar" src="http://www.fillmurray.com/g/100/104"/>
                                        <i className="pe-7s-angle-down f-large-8 vertical-align_middle"/>
                                    </a>
                                </li>
                            </ul>
                            <ul className="c-navbar__nav">
                                <li className="c-navbar__item">
                                    <Link to="dashboard" className="c-navbar__link-item">Dashboard</Link>
                                </li>
                                <li className="c-navbar__item">
                                    <Link to="overview" className="c-navbar__link-item">Ecosystem</Link>
                                </li>
                                <li className="c-navbar__item">
                                    <Link to="products" className="c-navbar__link-item">Profuct Families</Link>
                                </li>
                                <li className="c-navbar__item">
                                    <Link to="modules" className="c-navbar__link-item">Modules</Link>
                                </li>
                            </ul>
                            <ul className="c-navbar__nav c-navbar__nav-right">
                                <li className="c-navbar__item">
                                    <a className="c-navbar__icon-item">
                                        <i className="pe-7s-help2 f-large-15 vertical-align_middle"/>
                                    </a>
                                </li>
                                <li className="c-navbar__item">
                                    <a className="c-navbar__icon-item">
                                        <i className="pe-7s-bell f-large-15 vertical-align_middle"/>
                                        <span className="indicator"/>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                
                <RouteHandler/>
                
                <footer className="row">
                    <div className="large-12 columns">
                        <hr />
                        <div className="row">
                            <div className="large-6 columns">
                                <p>© Copyright</p>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        )
    }
}