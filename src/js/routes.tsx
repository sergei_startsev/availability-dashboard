/// <reference path="../../typings/react-router/react-router.d.ts" />

import * as React from "react";
import {Route} from "react-router";

import {AppTemplate} from "./app-template";
import {ProductsRoute} from "./tsx/routes/products-route";
import {OverviewRoute} from "./tsx/routes/overview-route";
import {ModulesRoute} from "./tsx/routes/modules-route";
import {DashboardRoute} from "./tsx/routes/dashboard-route";
import {Dashboard2Route} from "./tsx/routes/dashboard2-route";

// declare our routes and their hierarchy
export let Routes = (
    <Route name="root" path="/" handler={AppTemplate}>
        <Route name="dashboard" path="/" handler={DashboardRoute}/>
        <Route name="overview" path="overview" handler={OverviewRoute}/>
        <Route name="products" path="products" handler={ProductsRoute}/>
        <Route name="modules" path="modules" handler={ModulesRoute}/>
        <Route name="dashboard2" path="dashboard2" handler={Dashboard2Route}/>
    </Route>
);