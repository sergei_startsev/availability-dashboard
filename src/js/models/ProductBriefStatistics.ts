import {BriefStatistics} from "./briefStatistics";

export class ProductBriefStatistics{
    
    public productID: number;
    public productName: string;
    public env: string 
    public statistics: BriefStatistics;
    
    constructor(productID?: number,
                productName?: string,
                env?: string,
                statistics?: BriefStatistics
                ){
        this.productID = typeof productID !== 'undefined' ? productID : null;
        this.productName = typeof productName !== 'undefined' ? productName : "";
        this.env = typeof env !== 'undefined' ? env : "";
        this.statistics = typeof statistics !== 'undefined' ? statistics : new BriefStatistics();
    }
    
}