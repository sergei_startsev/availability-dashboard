# OVERVIEW
Tool for tracking Cobalt infrastructure availability.

# HOW TO

## PRECONDITION

* Clone this repo.
* Be sure that you've installed [Node.js](https://docs.npmjs.com/getting-started/installing-node) and [gulp-cli](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md).
* Restore packages from `package.json` using following command:

    `npm install`

from inside your app directory (i.e. where package.json is located). After you run that command `node_modules` directory should be added to your app directory.

## BUILD
To build the project run the following command:

`gulp dist`

Then you could find the distributed version in `dist` directory.

## START SERVER

To build project and start live reload server run the following command:

`gulp`

It builds the project, distributes sources to `dist` directory and runs live reload server. 
Deployed application will be available on the following address: [http://localhost:9999](http://localhost:9999).
