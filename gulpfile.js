/// <reference path="node_modules/tsconfig/tsconfig.d.ts"/>

var gulp = require('gulp');

var typescript = require('gulp-typescript');
var tsconfig = require('tsconfig');
var browserify = require("browserify");
var sass = require("gulp-sass");
var connect = require('gulp-connect');
var ftp = require('vinyl-ftp');
var gutil = require('gulp-util');
var del = require('del');
var fs = require('fs');

/**
 * Distribution folder
 */
var dist = "dist";

//---------------COMPILE_TYPESCRIPT------------------

gulp.task("compile_typescript", ["clean", "move"], function (done) {

    //load tsconfig.js from a project root folder
    tsconfig.load("", function (err, config) {

        config.compilerOptions.typescript = require('typescript');

        gulp.src(["src/js/**/*.ts", "src/js/**/*.tsx"])
            .pipe(typescript(config.compilerOptions))
            .pipe(gulp.dest('dist/js/'))
            .on('end', done);

    })

});

//---------------SASS------------------

gulp.task('sass', ["compile_typescript"], function (done) {

    return gulp.src("src/scss/**/*.scss")
        .pipe(sass())
        .pipe(gulp.dest("dist/css/"));

});

//---------------BROWSERIFY------------------

gulp.task("browserify", ["sass"], function (done) {

    var b = browserify();
    b.add("./dist/js/init.js");
    return b.bundle().pipe(fs.createWriteStream('dist/bundle.js'));

});

//---------------CLEAN------------------

gulp.task("clean", function (done) {

    del([
        'dist/**/*',
    ], done);

});

//---------------MOVE------------------

var filesToMove = [
    './src/bower_components/**/*.*',
    './src/icons/**/*.*',
    './src/app.css',
    './src/index.html'
];
gulp.task("move", ["clean"], function (done) {

    // the base option sets the relative root for the set of files,
    // preserving the folder structure
    return gulp.src(filesToMove, { base: './src/' })
        .pipe(gulp.dest('dist'));

});

//---------------START SERVER------------------

gulp.task('connect', ['browserify'], function() {
    connect.server({
        root: dist,
        port: 9999,
        livereload: true,
        fallback: dist + "/index.html"
    });
});

//---------------LIVERELOAD------------------

gulp.task('reload', ['browserify'], function() {
    gulp.src('./' + dist + '/**/*.*')
        .pipe(connect.reload());
});

//---------------WATCHER------------------

gulp.task('watch', ['browserify', 'connect'], function() {
    gulp.watch(['./src/**/*.*', '*.html'], ['reload']);
});

//---------------DEFAULT------------------

gulp.task('default', ['browserify', 'watch']);